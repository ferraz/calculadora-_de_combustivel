from calculo import Calculo

def main():
    print(
    """
    Esta aplicação tem como finalidade desmonstrar os valores que serão gastos com combustivel durante uma viagem,
    com base no consumo do seu veiculo, e com a distâcia determinada por você.
    """
    )

    print("Os combustiveis disponiveis para este cálculo são:")
    print(" 1 - Álcool")
    print(" 2 = Disel")
    print(" 3- Gasolina")

    print(" ")

    try:
        distancia = float(input("Distância em quilômetros a ser percorrida\n"))
        consumo = float(input("Consumo de combustivel do veiculo (km/1)\n"))
        calculo = Calculo()
        print(
            calculo.calcular_gasto(distancia, consumo)
        )
    except ValueError as erro:
        print("O valor recebido não é valido")

if __name__ == "__main__":
    main()
